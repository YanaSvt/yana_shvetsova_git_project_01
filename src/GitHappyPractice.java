import java.util.Scanner;

public class GitHappyPractice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your favorite day of the week");
        System.out.println("Why do you want to know the weather?");
        String day = input.nextLine();
        System.out.println("Your favorite day of the week is " + day);
        System.out.println("Nice weather today");

    }
}
